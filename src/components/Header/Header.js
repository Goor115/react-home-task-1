import './Header.scss';
import header_img from './img/header_img.png';
import React from 'react';

const Header = () => {
    return (
        <div>
            <p className="list">
                React is cool!!!
            </p>
            <img src={header_img} alt="logo-header"/>
        </div>
    );
};

export default Header;